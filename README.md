# mfti-test

## How to use:

1. `docker-compose build`
2. `docker-compose up -d db`
3. `docker-compose run app alembic upgrade head`
4. `docker-compose up`
5. Go to http://localhost:8000/docs
