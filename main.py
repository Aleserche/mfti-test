import uvicorn
from models import Product as ModelProduct
from schema import Product as SchemaProduct
from app import app
from db import db
from typing import Optional
from enum import Enum
from responses import JSONResponse, XMLResponse

class FormatName(str, Enum):
  json = "json"
  xml = "xml"

@app.post("/products/")
async def create_product(product: SchemaProduct):
  product_id = await ModelProduct.create(**product.dict())
  return {"product_id": product_id}

@app.put("/products/{id}")
async def update_product(id: int, product: SchemaProduct):
  await ModelProduct.update(id, product)

  return {"product_id": id}

@app.get("/products")
async def get_products(skip: int = 0, limit: int = 10):
  products = await ModelProduct.get_all(limit, skip)
  return products

@app.get("/products/{id}/{format_name}", responses={
  200: {
    "content": {
      "application/json": {},
      "application/xml": {},
    },
    "description": "Return the JSON item or XML.",
  }
})
async def get_product(id: int, format_name: FormatName):
  product = await ModelProduct.get(id)
  if format_name == FormatName.xml:
    return XMLResponse(content=SchemaProduct(**product).dict(), media_type="application/xml")

  return JSONResponse(content=SchemaProduct(**product).dict())

if __name__ == "__main__":
  uvicorn.run(app, host="0.0.0.0", port=8000)
