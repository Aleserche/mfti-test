from pydantic import BaseModel

class Product(BaseModel):
  name: str
  article: str
  code: int

  class Config:
    orm_mode = True
