from db import db, metadata, sqlalchemy

products = sqlalchemy.Table(
  "products",
  metadata,
  sqlalchemy.Column('id', sqlalchemy.Integer, primary_key=True),
  sqlalchemy.Column("name", sqlalchemy.String),
  sqlalchemy.Column("article", sqlalchemy.String),
  sqlalchemy.Column("code", sqlalchemy.Integer),
)

class Product:
  @classmethod
  async def get(cls, id):
    query = products.select().where(products.c.id == id)
    product = await db.fetch_one(query)
    return product

  @classmethod
  async def get_all(cls, limit = 0, offset = None):
    query = products.select().order_by(sqlalchemy.asc(products.c.id))

    if limit:
      query = query.limit(limit)
    if offset:
      query = query.offset(offset)

    product_list = await db.fetch_all(query)

    return product_list

  @classmethod
  async def create(cls, **product):
    query = products.insert().values(**product)
    product_id = await db.execute(query)
    return product_id

  @classmethod
  async def update(cls, id, product):
    query = products.update().where(products.c.id == id).values(**product.dict())
    return await db.execute(query)
