from fastapi.responses import JSONResponse
from starlette.responses import Response

import simplexml

class XMLResponse(Response):
  media_type = "text/xml"

  def render(self, content):
    return simplexml.dumps({'response': content}).encode("utf-8")
